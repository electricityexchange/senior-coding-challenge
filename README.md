# Electricity Exchange senior code challenge

## Objective

Create a simple command line (ascii text) API mashup of GitHub and Twitter APIs. Search
for "Reactive" projects on GitHub, then for each project search for tweets that mention it 
so we can see what was said about the project and who said it. You should output a summary 
of each project with a short list of recent tweets, in JSON format.

## Instructions

* Complete this assignment in javascript NodeJS (If you need to complete this in another language please let us know).
* We expect you to submit production quality code with all that this means to you.
* You will require a Twitter consumer key and secret to complete this.
* Any private information should not be included in your submission. However, you must make this information easily configurable via a configuration or properties file.
* Twitter API is rate limited so you should only try to retrieve tweets for 10 projects.
* To submit the code please follow the instructions provided
* We expect this challenge to be a git version controlled project. The repo contains a very basic .gitignore, please make any updates needed to this file as you see fit.

## Deadline
* You’ll have up to a week from now onwards to finish it. Please inform us with due consideration if you require more time to complete.
* Remember that we do not evaluate how fast you code: we will measure your code quality.

## More information

The GitHub API documentation

* https://developer.github.com/v3/search/#search-repositories Searching for reactive projects
* curl https://api.github.com/search/repositories\?q\=reactive 

The Twitter API documentation

* https://dev.twitter.com/oauth/application-only
* https://dev.twitter.com/oauth/overview/application-owner-access-tokens
* https://developer.twitter.com/en/docs/tweets/search/overview

## Submission

We expect this to be a git version controlled project. To submit your coding challenge, commit all your changes to the master branch and run the following command:

```
$ git bundle create coding-challenge.bundle HEAD master
```

Visit the form located [here](https://forms.gle/mfD9cmBebzdXmXiK9) to submit your coding-challenge.bundle file. We do our best to review and respond to submissions within a week.
Thanks for taking the time to do this coding challenge and here's hoping we talk soon!